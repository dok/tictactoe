package ru.dokwork.tictactoe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import ru.dokwork.tictactoe.TicTacToeField.State;
import static ru.dokwork.tictactoe.TicTacToeField.PLAYER_X;
import static ru.dokwork.tictactoe.TicTacToeField.PLAYER_O;

/**
 * Проверяется поведение игрового поля при выигрышном раскладе.
 */
@RunWith(Parameterized.class)
public class ExtendedGameFieldWinTest {

    @SuppressWarnings("rawtypes")
    @Parameters
    public static Collection getParameters() {
        return Arrays.asList(new Object[][] { { win_1, 3, PLAYER_X },
                { win_2, 3, PLAYER_O }, { win_3, 3, PLAYER_X } });
    }

    @Before
    public void setUp() {
        gamefield = new ExtendedGameField(field, winLength);
    }
    
    @Test
    public void testState() {
        State state = (winner == PLAYER_X) ? State.WIN_X : State.WIN_O;
        assertEquals(state, gamefield.getState());
    }

    @Test
    public void testWinner() {
        int player = gamefield.getWinner();
        assertEquals("Неверно определен победитель.", winner, player);
    }

    @Test
    public void testWinLine() {
        List<Cell> line = gamefield.getWinLine();
        assertNotNull("Массив выигрышных ячеек не определен.", line);
        assertEquals("Некоррекстная длина выигрышного ряда.", winLength, line.size());
        for (Cell m : line) {
            assertEquals("Описана неверная ячейка.", winner, field[m.row][m.col]);
        }
    }

    public ExtendedGameFieldWinTest(int[][] field, int winLength, int winner) {
        this.field = field;
        this.winLength = winLength;
        this.winner = winner;
    }

    private ExtendedGameField gamefield;
    private int[][] field;
    private int winner;
    private int winLength;

    private static int[][] win_1 = new int[][] { { 1, -1, 1 }, { -1, 1, -1 },
            { -1, 1, 1 } };
    private static int[][] win_2 = new int[][] { { 1, -1, 1 }, { 1, -1, 0 }, { 0, -1, 0 } };

    private static int[][] win_3 = new int[][] { { -1, 0, 0, 0, -1 }, { 0, 1, 0, 0, -1 },
            { 0, 0, 1, 0, 0 }, { 0, 0, 0, 1, 0 }, { 0, 0, 0, 0, 0 } };
}
