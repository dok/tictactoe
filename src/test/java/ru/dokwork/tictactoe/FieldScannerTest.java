package ru.dokwork.tictactoe;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import static ru.dokwork.tictactoe.FieldScanner.*;

public class FieldScannerTest {

    @Test
    public void testIsFullField() {
        assertFalse("emty is full.", isFullField(empty));
        assertFalse("example is full.", isFullField(example));
        assertTrue("full_x is empty.", isFullField(full_x));
        assertTrue("full_o is empty", isFullField(full_o));
    }

    @Test
    public void testScanRow() {
        /* Найдет ли выигрышные линии */
        List<Cell> line = null;
        int[] winIdexes = new int[] { 0, 4 };
        for (int index : winIdexes) {
            line = scanRow(example, 3, index);
            assertNotNull("In " + index + " line winLine not found.", line);
            assertEquals("Incorrect length of winLine in " + index + " line.", 3,
                    line.size());
        }
        /* Пропустит ли другие */
        int[] emptyIndexes = new int[] { 1, 2, 3 };
        for (int index : emptyIndexes) {
            line = scanRow(example, 3, index);
            assertNull("Line " + index + "is not empty.", line);
        }
    }

    @Test
    public void testScanCol() {
        /* Найдет ли выигрышные столбцы */
        List<Cell> line = null;
        int[] winIdexes = new int[] { 0, 4 };
        for (int index : winIdexes) {
            line = scanCol(example, 3, index);
            assertNotNull("In " + index + " line winLine not found.", line);
            assertEquals("Incorrect length of winLine in " + index + " line.", 3,
                    line.size());
        }
        /* Пропустит ли другие */
        int[] emptyIndexes = new int[] { 1, 2, 3 };
        for (int index : emptyIndexes) {
            line = scanCol(example, 3, index);
            assertNull("Line " + index + "is not empty.", line);
        }
    }

    @Test
    public void testScanUpDiagonal() {
        /* Найдет ли выигрышные диагонали */
        List<Cell> line = null;
        int[] winIdexes = new int[] { 0, 2 };
        // слева направо
        for (int index : winIdexes) {
            line = scanUpDiagonal(true, example, 3, index);
            assertNotNull("In " + index + " line winLine not found.", line);
            assertEquals("Incorrect length of winLine in " + index + " line.", 3,
                    line.size());
        }
        // справо налево
        int i = 4;
        line = scanUpDiagonal(false, example, 3, i);
        assertNotNull("In " + i + " line winLine not found.", line);
        assertEquals("Incorrect length of winLine in " + i + " line.", 3, line.size());

        /* Пропустит ли другие */
        int[] emptyIndexes = new int[] { 1, 3 };
        for (int index : emptyIndexes) {
            // слева направо
            line = scanUpDiagonal(true, example, 3, index);
            assertNull("Left-right diagonal " + index + " is not empty.", line);
            // справо налево
            line = scanUpDiagonal(false, example, 3, index);
            assertNull("Right-left diagonal " + index + " is not empty.", line);
        }
    }

    @Test
    public void testScanDownDiagonal() {
        /* Найдет ли выигрышные диагонали */
        List<Cell> line = null;
        // слева направо
        int index = 0;
        line = scanDownDiagonal(true, example, 3, index);
        assertNotNull("In " + index + " line winLine not found.", line);
        assertEquals("Incorrect length of winLine in " + index + " line.", 3, line.size());
        // справа налево
        index = 4;
        line = scanDownDiagonal(false, example, 3, index);
        assertNotNull("In " + index + " line winLine not found.", line);
        assertEquals("Incorrect length of winLine in " + index + " line.", 3, line.size());
        
        /* Пропустит ли другие */
        int[] emptyIndexes = new int[] { 1, 2, 3 };
        for (int i : emptyIndexes) {
            // слева направо
            line = scanDownDiagonal(true, example, 3, i);
            assertNull("Left-right diagonal " + i + " is not empty.", line);
            // справо налево
            line = scanDownDiagonal(false, example, 3, i);
            assertNull("Right-left diagonal " + i + " is not empty.", line);
        }
        
    }
    
    /** Замечена ошибка в сканировании второй главной диагонали (/) */
    @Test
    public void testExample2() {
        int index = 2;
        List<Cell> line = FieldScanner.scanUpDiagonal(false, example2, 3, index);
        assertNull(line);
        index = 0;
        line = FieldScanner.scanDownDiagonal(true, example2, 3, index);
        assertNull(line);
    }
    
    private static int[][] empty = new int[][] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
    private static int[][] full_x = new int[][] { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
    private static int[][] full_o = new int[][] { { -1, -1, -1 }, { -1, -1, -1 },
            { -1, -1, -1 } };
    private static int[][] example = new int[][] { { 0, 1, 1, 1, 0 }, { -1, 1, 0, 1, 1 },
            { -1, 0, 1, 0, 1 }, { -1, 1, 0, 1, 1 }, { 0, -1, -1, -1, 0 } };
    
    private static int[][] example2 = new int[][] {{1, 1, -1}, {0, -1, 1}, {0, -1, 1}};

}
