package ru.dokwork.tictactoe.swing;

import java.util.EventListener;

public interface CellClickedEventListener extends EventListener {
    /**
     * Реакция слушателя на клик по ячейке.
     * 
     * @param source
     *            игровое поле с ячейкой.
     * @param row
     *            строка игрового поля с ячейкой.
     * @param col
     *            столбец игрового поля с ячейкой.
     */
    void cellClicked(Object source, int row, int col);
}
