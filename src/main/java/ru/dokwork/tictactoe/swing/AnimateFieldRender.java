package ru.dokwork.tictactoe.swing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.JComponent;

import ru.dokwork.tictactoe.Cell;
import ru.dokwork.tictactoe.Move;
import ru.dokwork.tictactoe.TicTacToeField;

public class AnimateFieldRender implements GameFiledRender, ComponentListener {

    @Override
    public void paint(JTicTacToeField jfield, Graphics g) {
        if (buffer == null || !compareSize(jfield, buffer)) {
            paintComponentToBuffer(jfield);
        }
        int x1 = g.getClipBounds().x;
        int y1 = g.getClipBounds().y;
        int x2 = x1 + g.getClipBounds().width;
        int y2 = y1 + g.getClipBounds().height;
        g.drawImage(buffer, x1, y1, x2, y2, x1, y1, x2, y2, null);
    }

    @Override
    public void clean() {
        buffer = null;
    }

    @Override
    public void animateMove(int player, Move move, JTicTacToeField jfield, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        double cellWSize = jfield.getCellWidth();
        double cellHSize = jfield.getCellHeight();
        int x = (int) Math.round(move.getCol() * cellWSize);
        int y = (int) Math.round(move.getRow() * cellHSize);
        switch (player) {
        case JTicTacToeField.FIRST_PLAYER:
            animateX(x, y, cellWSize, cellHSize, g2);
            break;
        case JTicTacToeField.SECOND_PLAYER:
            animateO(x, y, cellWSize, cellHSize, g2);
            break;
        }
        /* Результат фиксируется в буфере */
        Graphics2D bg = buffer.createGraphics();
        bg.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        paintCell(player, x, y, cellWSize, cellHSize, bg);
        bg.dispose();
        /* Вызывается для сглаживания артефактов, получающихся из-за поэтапной отрисовки */
        // paintCell(player, x, y, cellWSize, cellHSize, g2);
        g2.setClip(x, y, (int) cellWSize, (int) cellHSize);
        paint(jfield, g2);
    }

    @Override
    public void animateWinLine(List<Cell> winLine, JTicTacToeField jfield, Graphics g) {
        double cellWSize = jfield.getCellWidth();
        double cellHSize = jfield.getCellHeight();
        double halfW = cellWSize / 2;
        double halfH = cellHSize / 2;

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        float strokeSize = getStrokeSize(cellWSize, cellHSize);
        g2.setStroke(new BasicStroke(strokeSize));
        g2.setColor(colorWinLine);

        Cell s = winLine.get(0);
        Cell e = (winLine.size() > 0) ? winLine.get(winLine.size() - 1) : s;
        int x1 = (int) Math.round(s.col * cellWSize + halfW);
        int y1 = (int) Math.round(s.row * cellHSize + halfH);
        int x2 = (int) Math.round(e.col * cellWSize + halfW);
        int y2 = (int) Math.round(e.row * cellHSize + halfH);
        animateLine(x1, y1, x2, y2, 40, 5, g2);
        /* Результат фиксируется в буфере */
        Graphics2D bg = buffer.createGraphics();
        bg.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        paintWinLine(winLine, cellWSize, cellHSize, bg);
        bg.dispose();
    }

    @Override
    public void componentResized(ComponentEvent e) {
        if (e.getComponent() instanceof JTicTacToeField) {
            JTicTacToeField jfield = (JTicTacToeField) e.getComponent();
            paintComponentToBuffer(jfield);
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    private void paintComponentToBuffer(JTicTacToeField jfield) {
        Dimension dim = jfield.getSize();
        // XXX В Ubuntu 10.04 было замечено отрицательное значение ширины компонента.
        buffer = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = buffer.createGraphics();
        g2.setClip(0, 0, buffer.getWidth(), buffer.getHeight());
        paintComponent(jfield, g2);
        g2.dispose();
    }

    private boolean compareSize(JComponent c, BufferedImage img) {
        Dimension dim = c.getSize();
        return (dim.width == img.getWidth()) && (dim.height == img.getHeight());
    }

    private void paintComponent(JTicTacToeField jfield, Graphics2D g2) {
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        /* Paint field */
        g2.setColor(background);
        g2.fillRect(g2.getClipBounds().x, g2.getClipBounds().y, g2.getClipBounds().width,
                g2.getClipBounds().height);
        g2.setColor(border);
        g2.setStroke(new BasicStroke(1f));
        g2.drawRect(g2.getClipBounds().x, g2.getClipBounds().y,
                g2.getClipBounds().width - 1, g2.getClipBounds().height - 1);
        if (jfield == null || jfield.getField() == null) {
            return;
        }
        int x = 0;
        int y = 0;
        double cellWSize = jfield.getCellWidth();
        double cellHSize = jfield.getCellHeight();
        TicTacToeField field = jfield.getField();
        for (int i = 1; i < field.getRowCount(); i++) {
            y = (int) Math.round(cellHSize * i);
            g2.drawLine(0, y, g2.getClipBounds().width, y);
            x = (int) Math.round(cellWSize * i);
            g2.drawLine(x, 0, x, g2.getClipBounds().height);
        }
        /* Paint cells */
        for (int i = 0; i < field.getRowCount(); i++) {
            for (int j = 0; j < field.getColCount(); j++) {
                x = (int) Math.round(cellWSize * j);
                y = (int) Math.round(cellHSize * i);
                paintCell(field.get(i, j), x, y, cellWSize, cellHSize, g2);
            }
        }
        /* Paint winline */
        paintWinLine(jfield.getField().getWinLine(), jfield.getCellWidth(),
                jfield.getCellHeight(), g2);
    }

    /**
     * Рисует ячейку.
     * 
     * @param cell
     *            значение поля в ячейке.
     * @param x
     *            горизонтальная координата верхнего левого угла ячейки.
     * @param y
     *            вертикальная координата верхнего левого угла ячейки.
     * @param width
     *            ширина ячейки.
     * @param height
     *            высота ячейки.
     * @param g2
     */
    private void paintCell(int cell, int x, int y, double width, double height,
            Graphics2D g2) {
        float strokeSize = getStrokeSize(width, height);
        g2.setStroke(new BasicStroke(strokeSize));
        x += strokeSize;
        y += strokeSize;
        width -= 2 * strokeSize;
        height -= 2 * strokeSize;

        switch (cell) {
        case JTicTacToeField.FIRST_PLAYER:
            g2.setColor(colorX);
            g2.drawLine(x, y, (int) (x + width), (int) (y + height));
            g2.drawLine((int) (x + width), y, x, (int) (y + height));
            break;
        case JTicTacToeField.SECOND_PLAYER:
            g2.setColor(colorO);
            g2.drawArc(x, y, (int) width, (int) height, 0, 360);
            break;
        }
    }

    private void paintWinLine(List<Cell> winLine, double cellWidth, double cellHeight,
            Graphics2D g2) {
        if (winLine == null) {
            return;
        }
        double halfW = cellWidth / 2;
        double halfH = cellHeight / 2;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        float strokeSize = getStrokeSize(cellWidth, cellHeight);
        g2.setStroke(new BasicStroke(strokeSize));
        g2.setColor(colorWinLine);
        Cell s = winLine.get(0);
        Cell e = (winLine.size() > 0) ? winLine.get(winLine.size() - 1) : s;
        int x1 = (int) Math.round(s.col * cellWidth + halfW);
        int y1 = (int) Math.round(s.row * cellHeight + halfH);
        int x2 = (int) Math.round(e.col * cellWidth + halfW);
        int y2 = (int) Math.round(e.row * cellHeight + halfH);
        g2.drawLine(x1, y1, x2, y2);
    }

    private void animateO(int x, int y, double width, double height, Graphics2D g2) {
        float strokeSize = getStrokeSize(width, height);
        g2.setStroke(new BasicStroke(strokeSize));
        g2.setColor(colorO);
        x += strokeSize;
        y += strokeSize;
        width -= 2 * strokeSize;
        height -= 2 * strokeSize;
        int count = 10;
        int startAngle = 440;
        int arcAngle = 360 / count;
        for (int i = 0; i < count; i++) {
            g2.drawArc(x, y, (int) width, (int) height, startAngle, -arcAngle);
            startAngle -= arcAngle;
            sleep(40);
        }
    }

    private void animateX(int x, int y, double width, double height, Graphics2D g2) {
        float strokeSize = getStrokeSize(width, height);
        g2.setStroke(new BasicStroke(strokeSize));
        g2.setColor(colorX);
        x += strokeSize;
        y += strokeSize;
        width -= 2 * strokeSize;
        height -= 2 * strokeSize;
        animateLine(x, y, (int) (x + width), (int) (y + height), 100, 3, g2);
        sleep(100);
        animateLine((int) (x + width), y, x, (int) (y + height), 100, 3, g2);
    }

    private void animateLine(int x1, int y1, int x2, int y2, long time, int iterations,
            Graphics2D g2) {
        double dx = (x2 - x1) / iterations;
        double dy = (y2 - y1) / iterations;
        long delay = time / iterations;
        for (int i = 0; i < iterations; i++) {
            x2 = (int) Math.round(x1 + dx);
            y2 = (int) Math.round(y1 + dy);
            g2.drawLine(x1, y1, x2, y2);
            x1 = (int) Math.round(x1 + dx);
            y1 = (int) Math.round(y1 + dy);
            sleep(delay);
        }
    }

    private float getStrokeSize(double width, double height) {
        return (float) Math.min(width, height) / 5;
    }

    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private BufferedImage buffer = null;

    private Color border = Color.black;
    private Color background = Color.white;
    private Color colorX = Color.red.darker();
    private Color colorO = Color.green.darker();
    private Color colorWinLine = Color.blue.darker();
}
