package ru.dokwork.tictactoe.swing;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;

import ru.dokwork.tictactoe.Cell;
import ru.dokwork.tictactoe.ExtendedGameField;
import ru.dokwork.tictactoe.Move;
import ru.dokwork.tictactoe.TicTacToeField;

/**
 * Графическое представление игрового поля для иргры Крестики-нолики.
 */
public class JTicTacToeField extends JComponent {

    public double getCellWidth() {
        if (field == null) {
            return 0;
        }
        return getSize().getWidth() / field.getColCount();
    }

    public double getCellHeight() {
        if (field == null) {
            return 0;
        }
        return getSize().getHeight() / field.getRowCount();
    }

    public TicTacToeField getField() {
        return field;
    }

    public void setField(TicTacToeField field) {
        render.clean();
        this.field = field;
        this.repaint();
    }

    public void animateMove(Move move, int player) {
        Graphics g = getGraphics();
        render.animateMove(player, move, this, g);
        g.dispose();
    }

    public void animateWinLine(List<Cell> winLine) {
        Graphics g = getGraphics();
        render.animateWinLine(winLine, this, g);
        g.dispose();
    }

    public void addCellClickListener(CellClickedEventListener listener) {
        listeners.add(listener);
    }

    public void removeCellClickListener(CellClickedEventListener listener) {
        listeners.remove(listener);
    }

    public JTicTacToeField() {
        listeners = new LinkedList<CellClickedEventListener>();
        render = new AnimateFieldRender();
        addComponentListener((AnimateFieldRender) render);
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                onMouseClicked(e.getX(), e.getY());
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        render.paint(this, g);
    }

    private void onMouseClicked(int x, int y) {
        int col = (int) (x / getCellWidth());
        int row = (int) (y / getCellHeight());
        for (CellClickedEventListener l : listeners) {
            l.cellClicked(this, row, col);
        }
    }

    private TicTacToeField field;

    private final GameFiledRender render;

    private LinkedList<CellClickedEventListener> listeners;

    public static final int FIRST_PLAYER = ExtendedGameField.PLAYER_X;

    public static final int SECOND_PLAYER = ExtendedGameField.PLAYER_O;

    private static final long serialVersionUID = -236492987983945601L;
}
