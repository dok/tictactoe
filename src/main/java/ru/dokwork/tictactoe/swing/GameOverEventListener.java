package ru.dokwork.tictactoe.swing;

import java.util.EventListener;

/**
 * Интерфейс слушателя событий завершения игры.
 */
public interface GameOverEventListener extends EventListener {
    void gameOver(GameOverEvent event);
}
