package ru.dokwork.tictactoe.swing;

import java.util.EventObject;

/**
 * Информирует о завершении игры.
 */
public class GameOverEvent extends EventObject {
    /**
     * Возможные варианты завершения игры.
     */
    public enum Reasons {
        /**
         * Победа
         */
        WIN,
        /**
         * Ничья
         */
        DRAWN
    }

    /**
     * Возвращает причину завершения игры.
     */
    public Reasons getReason() {
        return reason;
    }

    /**
     * Возвращает игрока, чьим ходом закончилась игра.
     */
    public int getPlayer() {
        return player;
    }

    /**
     * Возвращает номер столбца начала собранного ряда.
     */
    public int getCol1() {
        return col1;
    }

    /**
     * Возвращает номер строки начала собранного ряда.
     */
    public int getRow1() {
        return row1;
    }

    /**
     * Возвращает номер столбца конца собранного ряда.
     */
    public int getCol2() {
        return col2;
    }

    /**
     * Возвращает номер строки конца собранного ряда.
     */
    public int getRow2() {
        return row2;
    }

    /**
     * Создает объект, описывающий событие завершения игры.
     *
     * @param source объект, сгенерировавший событие.
     * @param reason причина завершения игры.
     * @param player игрок, на чьем ходу завершилась игра.
     * @param col1     номер столбца начала собранного ряда.
     * @param row1     номер строки начала собранного ряда.
     * @param col2     номер столбца конца собранного ряда.
     * @param row2     номер строки конца собранного ряда.
     * @throws IllegalArgumentException if source is null.
     */
    public GameOverEvent(Object source, Reasons reason, int player,
                         int col1, int row1, int col2, int row2) {
        super(source);
        this.reason = reason;
        this.player = player;
        this.col1 = col1;
        this.row1 = row1;
        this.col2 = col2;
        this.row2 = row2;
    }

    /**
     * Создает объект, описывающий событие завершения игры в случае ничьей.
     *
     * @param source объект, сгенерировавший событие.
     */
    public GameOverEvent(Object source) {
        this(source, Reasons.DRAWN, 0, 0, 0, 0, 0);
    }

    private Reasons reason;
    private int player;
    private int col1, row1, col2, row2;

    private static final long serialVersionUID = 32624762271636387L;
}
