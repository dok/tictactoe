package ru.dokwork.tictactoe.swing;

import java.awt.BorderLayout;

import javax.swing.JApplet;
import javax.swing.SwingUtilities;

import ru.dokwork.tictactoe.TicTacToeGame;

public class TicTacToeApplet extends JApplet implements Runnable {

    @Override
    public void init() {
        try {
            SwingUtilities.invokeAndWait(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() {
        TicTacToeGame game = new TicTacToeGame(jTicTacToe, size, winLength, depth);
        game.newGame();
    }

    @Override
    public void run() {
        jTicTacToe = new JTicTacToe();
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(jTicTacToe, BorderLayout.CENTER);
    }

    private JTicTacToe jTicTacToe;

    private int size = 10;

    private int winLength = 5;
    
    private int depth = 2;

    private static final long serialVersionUID = 2339435347302024306L;
}
