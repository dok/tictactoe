package ru.dokwork.tictactoe.swing;

import java.awt.Graphics;
import java.util.List;

import ru.dokwork.tictactoe.Cell;
import ru.dokwork.tictactoe.Move;

public interface GameFiledRender {
    
    public abstract void paint(JTicTacToeField jfield, Graphics g);
    
    public abstract void clean();

    public abstract void animateMove(int player, Move move, JTicTacToeField jfield,
            Graphics g);

    public abstract void animateWinLine(List<Cell> winLine, JTicTacToeField jfield,
            Graphics g);

}