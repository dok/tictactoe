package ru.dokwork.tictactoe.swing;

import java.awt.EventQueue;

import javax.swing.JFrame;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.dokwork.tictactoe.TicTacToeGame;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

public class App implements IParameterValidator, Runnable {
    /**
     * Launch the application.
     */
    public static void main(final String[] args) {
        App app = new App();
        app.initialize(args);
        EventQueue.invokeLater(app);
    }

    /** {@inheritDoc} */
    @Override
    public void validate(String name, String value) throws ParameterException {
        int val = Integer.parseInt(value);
        if (name.equals("-w") && val < 3) {
            throw new ParameterException("Играть до " + value + " совсем не интересно.");
        }
        if (name.equals("-w") && val > 10) {
            throw new ParameterException(
                    "Увы. Используемые оценочные функции растут слишком быстро. При таком значении выигрышной длины оценки превысят допустимые границы.");
        }
        if (name.equals("-s") && val < 3) {
            throw new ParameterException("Слишком маленькое поле.");
        }
        if (name.equals("-d") && val < 0) {
            throw new ParameterException("Глубина раскрытия дерева не может быть отрицательной.");
        }
    }

    public void initialize(String[] args) {
        if (args.length == 1 && args[0].equals("-h")) {
            jc.usage();
            System.exit(1);
        }
        jc.parse(args);
        if (winLength > size) {
            throw new ParameterException(
                    "Длина выигрышной комбинации должна быть меньше или равна размеру игрового поля.");
        }
        Level lvl = Level.toLevel(level, Level.OFF);
        BasicConfigurator.configure(new ConsoleAppender(new PatternLayout("%m%n")));
        org.apache.log4j.Logger.getRootLogger().setLevel(lvl);
    }

    public void run() {
        final JTicTacToe jtictactoe = new JTicTacToe();
        JFrame frame = new JFrame() {
            {
                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setLocationRelativeTo(null);
                setSize(300, 320);
                setTitle("TicTacToe");
                add(jtictactoe);
            }
            private static final long serialVersionUID = -2850496084534468143L;
        };
        TicTacToeGame game = new TicTacToeGame(jtictactoe, size, winLength, depth);
        game.newGame();
        frame.setVisible(true);
    }

    public App() {
        jc = new JCommander();
        jc.addObject(this);
        jc.setProgramName("TicTacToe");
    }

    @Parameter(names = { "-l", "--level" }, description = "Задает уровень вывода дополнительной информации: OFF, FATAL, ERROR, WARN, INFO, DEBUG и ALL")
    private String level = "OFF";

    @Parameter(names = { "-s", "--size" }, validateWith = App.class, description = "Задает размерность поля.")
    private int size = 10;

    @Parameter(names = { "-w", "--win" }, validateWith = App.class, description = "Задает длину выигрышной комбинации.")
    private int winLength = 5;
    
    @Parameter(names = { "-d", "--depth" }, validateWith = App.class, description = "Задает глубину раскрытия дерева игры. При 0, для оценки ходов используется только эвристика.")
    private int depth = 2;

    private JCommander jc;

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(App.class);
}
