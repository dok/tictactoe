package ru.dokwork.tictactoe.swing;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.dokwork.tictactoe.Cell;
import ru.dokwork.tictactoe.Move;
import ru.dokwork.tictactoe.TicTacToeField;
import ru.dokwork.tictactoe.TicTacToeField.State;
import ru.dokwork.tictactoe.TicTacToeView;

/**
 * Компонент, представляющий игру в крестики нолики.
 * 
 * @author dok
 * 
 */
public class JTicTacToe extends JComponent implements TicTacToeView {

    private JTicTacToeField jGameField;

    @Override
    public void updateWithField(TicTacToeField field) {
        jGameField.setField(field);
    }

    @Override
    public void gameOver(State result, List<Cell> winLine) {
        if (result.equals(State.DRAW)) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                LOG.error("", e);
            }
            Icon icon = new ImageIcon(handshake);
            JOptionPane.showMessageDialog(this, "Ничья.", "Конец игры",
                    JOptionPane.INFORMATION_MESSAGE, icon);
            return;
        } else {
            String winner = (result == State.WIN_X) ? "КРЕСТИКИ" : "НОЛИКИ";
            jGameField.animateWinLine(winLine);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                LOG.error("", e);
            }
            Image img = (result == State.WIN_X) ? imageX : imageO;
            Icon icon = new ImageIcon(img);
            JOptionPane.showMessageDialog(this, "Победили " + winner, "Конец игры",
                    JOptionPane.INFORMATION_MESSAGE, icon);
        }
    }

    @Override
    public void addCellClickListener(CellClickedEventListener listener) {
        jGameField.addCellClickListener(listener);
    }

    @Override
    public void animateMove(Move move, int player) {
        jGameField.animateMove(move, player);
    }

    /**
     * Create the frame.
     */
    public JTicTacToe() {
        try {
            loadImages();
        } catch (IOException e1) {
            LOG.error("", e1);
        }
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setLayout(new BorderLayout(0, 0));

        final JLabel info = new JLabel("http://www.dokwork.ru/2012/11/tictactoe.html");
        this.add(info, BorderLayout.SOUTH);

        jGameField = new JTicTacToeField();
        jGameField.addMouseMotionListener(new MouseAdapter() {
            public void mouseMoved(MouseEvent e) {
                int c = (int) (e.getX() / jGameField.getCellWidth());
                int r = (int) (e.getY() / jGameField.getCellHeight());
                info.setText("r: " + r + " c: " + c);
            }
        });
        this.add(jGameField, BorderLayout.CENTER);
    }

    private void loadImages() throws IOException {
        this.imageX = ImageIO.read(getClass().getResourceAsStream("tic.png"));
        this.imageO = ImageIO.read(getClass().getResourceAsStream("tac.png"));
        this.handshake = ImageIO.read(getClass().getResourceAsStream("handshake.png"));
    }

    private Image imageX;

    private Image imageO;

    private Image handshake;

    private static final Logger LOG = LoggerFactory.getLogger(JTicTacToe.class);

    private static final long serialVersionUID = 8352958539039666548L;
}
