package ru.dokwork.tictactoe;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.dokwork.tictactoe.swing.CellClickedEventListener;
import ru.dokwork.zerosumgame.AlphaBetaSolver;
import ru.dokwork.zerosumgame.Rules;
import ru.dokwork.zerosumgame.Solver;

public class TicTacToeGame implements CellClickedEventListener {

    /** {@inheritDoc} */
    @Override
    public void cellClicked(Object source, int row, int col) {
        try {
            movePlayer(new Move(row, col));
            moveAI();
        } catch (IllegalMoveException e) {
            LOG.error("Недопустимый ход");
        }
        checkGameState();
    }

    public void newGame() {
        LOG.info("New game. Field size = " + size + " Winlength = " + winLength
                + " Max depth = " + maxDepth);
        field = new ExtendedGameField(size, winLength);
        rules = field;
        ExpertHeuristic heuristic = new ExpertHeuristic(PLAYER);
        solver = new AlphaBetaSolver<ExtendedGameField>(rules, PLAYER, field, heuristic,
                maxDepth);
        view.updateWithField(field);
    }

    public TicTacToeGame(TicTacToeView view, int size, int winLength, int depth) {
        this.view = view;
        this.size = size;
        this.maxDepth = depth;
        this.winLength = winLength;
        view.addCellClickListener(this);
    }

    private void movePlayer(Move m) {
        if (field.isGameOver()) {
            return;
        }
        LOG.info("Player move to " + m);
        field.doMove(m, PLAYER);
        view.animateMove(m, PLAYER);
    }

    private void moveAI() {
        if (field.isGameOver()) {
            return;
        }
        Move m = solver.getBestMove();
        LOG.info("AI move to " + m + "\n");
        field.doMove(m, AI);
        view.animateMove(m, AI);
    }

    private void checkGameState() {
        if (field.isGameOver()) {
            List<Cell> winLine = field.getWinLine();
            view.gameOver(field.getState(), winLine);
            newGame();
        }
    }

    private Rules rules;

    private ExtendedGameField field;

    private Solver<ExtendedGameField> solver;

    private TicTacToeView view;

    private final int size;

    private final int winLength;

    private final int maxDepth;

    private static final int PLAYER = TicTacToeField.PLAYER_X;

    private static final int AI = TicTacToeField.PLAYER_O;

    private static final Logger LOG = LoggerFactory.getLogger(TicTacToeGame.class);
}
