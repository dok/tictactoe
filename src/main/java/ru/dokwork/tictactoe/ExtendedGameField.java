package ru.dokwork.tictactoe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.dokwork.tictactoe.FieldScanner.Line;
import ru.dokwork.tictactoe.FieldScanner.Score;

/**
 * Расширенное представление игрового поля, в котором состояние пересматривается
 * относительно хода игрока, и возможные ходы предоставялются только в окрестности уже
 * занятых клеток.
 * 
 * @author dok
 * 
 */
public class ExtendedGameField extends TicTacToeField {
    @Override
    public Move[] getMoves(int player) {
        if (moves == null || moves.size() == 0) {
            return super.getMoves(player);
        }
        Move[] a = new Move[moves.size()];
        return moves.toArray(a);
    }

    @Override
    public void doMove(Move move, int player) {
        if (field[move.row][move.col] != 0) {
            throw new IllegalMoveException(move.row, move.col, player);
        }
        if (getState() != State.NONE) {
            throw new IllegalMoveException("Игра уже закончена.", move.row, move.col, player);
        }
        field[move.row][move.col] = player;

        Collection<Move> mvs = new ArrayList<Move>();
        Collection<Move> ngbrs = getNeighboringMoves(move);
        for (Move m : ngbrs) {
            if (!moves.contains(m)) {
                moves.add(m);
                mvs.add(m);
            }
        }
        prevMoves.put(move, mvs);
        moves.remove(move);
        showMoves();
        reviewState(move, player);
    }

    @Override
    public void undoMove(Move m, int player) {
        super.undoMove(m, player);
        Collection<Move> mvs = prevMoves.get(m);
        prevMoves.remove(m);
        moves.removeAll(mvs);
        moves.add(m);
    }

    public Collection<Move> getNeighboringMoves(Move m) {
        Collection<Move> mvs = new ArrayList<Move>();
        int row = (m.row > 0) ? m.row - 1 : 0;
        for (; row < getRowCount() && row < (m.row + 2); row++) {
            int col = (m.col > 0) ? m.col - 1 : 0;
            for (; col < getColCount() && col < (m.col + 2); col++) {
                if (get(row, col) == 0) {
                    mvs.add(new Move(row, col));
                }
            }
        }
        return mvs;
    }

    public ExtendedGameField(int size, int winLength) {
        super(size, winLength);
        moves = new HashSet<Move>();
        prevMoves = new HashMap<Move, Collection<Move>>();
    }

    public ExtendedGameField(int[][] field, int winLength) {
        super(field.length, winLength);
        moves = new HashSet<Move>();
        prevMoves = new HashMap<Move, Collection<Move>>();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                if (field[i][j] == 0) {
                    continue;
                }
                doMove(new Move(i, j), field[i][j]);
            }
        }
    }

    /**
     * Проверяет, не изменилось ли состояние игрового поля в результате хода.
     */
    private void reviewState(Move m, int player) {
        for (Line line : Line.values()) {
            Score score = FieldScanner.scoreLine(line, field, winLength, m, player);
            if (score.inrow >= winLength) {
                State state = (player == PLAYER_X) ? State.WIN_X : State.WIN_O;
                setState(state);
                return;
            }
        }
        if (FieldScanner.isFullField(field)) {
            setState(State.DRAW);
            return;
        }
    }

    private void showMoves() {
        for (Move m : moves) {
            LOG.trace(" : " + m);
        }
    }

    private HashSet<Move> moves;

    private Map<Move, Collection<Move>> prevMoves;

    private static final Logger LOG = LoggerFactory.getLogger(ExtendedGameField.class);
}
