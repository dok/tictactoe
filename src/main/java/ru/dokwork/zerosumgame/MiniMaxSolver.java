package ru.dokwork.zerosumgame;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.dokwork.tictactoe.Move;

/**
 * Выполняет выбор наилучшего хода с помощью минимаксной процедуры.
 * 
 * @author dok
 * 
 */
public class MiniMaxSolver<TField extends Field> extends Solver<TField> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected int score(TField field, int player, Move m) {
        return min(field, m, player, 1);
    }

    public int max(TField field, Move m, int player, int depth) {
        if (field.isGameOver() || depth == maxDepth) {
            int score = heuristic.score(field, player, m);
            return score;
        }
        int score = Integer.MIN_VALUE;
        Move[] moves = rules.getMoves(player);
        for (Move move : moves) {
            field.doMove(move, player);
            int res = min(field, move, -player, depth + 1);
            field.undoMove(move, player);
            score = (res > score) ? res : score;
        }
        return score;
    }

    public int min(TField field, Move m, int player, int depth) {
        if (field.isGameOver() || depth == maxDepth) {
            int score = -heuristic.score(field, player, m);
            return score;
        }
        int score = Integer.MAX_VALUE;
        Move[] moves = rules.getMoves(player);
        for (Move move : moves) {
            field.doMove(move, player);
            int res = max(field, move, -player, depth + 1);
            field.undoMove(move, player);
            score = (res < score) ? res : score;
        }
        return score;
    }

    /**
     * 
     * @param rules
     * @param player
     * @param field
     * @param heuristic
     *            объект для эвристической оценки состояния игрового поля.
     * @param maxDepth
     */
    public MiniMaxSolver(Rules rules, int player, TField field,
            Heuristic<TField> heuristic, int maxDepth) {
        super(rules, player, field);
        if (rules == null) {
            throw new IllegalArgumentException("Rules can`t be null.");
        }
        this.heuristic = heuristic;
        this.maxDepth = maxDepth;
        LOG.debug("MiniMaxSolver created with " + heuristic.getClass().getName()
                + " heuristic.");
    }

    private int maxDepth;
    
    private final Heuristic<TField> heuristic;

    private static final Logger LOG = LoggerFactory.getLogger(MiniMaxSolver.class);
}
