package ru.dokwork.zerosumgame;

import ru.dokwork.tictactoe.Move;

/**
 * Эвристическая оценка состояния игрового поля.
 */
public interface Heuristic<TField extends Field> {
    /**
     * Возвращает оценку состояния игрового поля.
     * 
     * @param field
     *            состояние игрового поля, порожденное ходом игрока player.
     * @param player
     *            идентификатор игрока для которого происходит оценка.
     * @param move
     *            ход, который привел к оцениваемому состоянию.
     * @return оценка состояния игрового поля.
     */
    public int score(TField field, int player, Move move);
}
